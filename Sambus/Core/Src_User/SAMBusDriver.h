/*
 * SAMBusDriver.h
 *
 *  Created on: Jan 11, 2022
 *      Author: micha
 */

#ifndef SRC_USER_SAMBUSDRIVER_H_
#define SRC_USER_SAMBUSDRIVER_H_
#include "stm32f3xx_hal.h"

struct SambusFrame
{
	uint8_t type;
	uint8_t address;
	uint8_t length;

};

enum class SambusFlag
{
  OK,
  ERROR_FRAME_INVALID,
  ERROR_CRC,
  ERROR_NOFRAME
};


class SAMBUSDriver
{
public:


  void Init();

  void UART_Transmit(uint8_t *sendData, int DataSize);

  int UART_Recieve(uint8_t *recData, int DataSize);

  SambusFlag Encode(const SambusFrame &TxFrame, uint8_t *sendData, int DataSize, uint8_t *destData, int PacketSize);
  //int status = Sambus.Encode2(Frame, sendData, 10, destData, 256);

  SambusFlag Decode(SambusFrame &RxFrame, uint8_t *recData, int DataSize, uint8_t *packetData, int PacketSize);
  //	int status2 = Sambus.Decode2(destData, 256, &recFrame, recData, 256);

  void SAMBUS();
};

#endif /* SRC_USER_SAMBUSDRIVER_H_ */
