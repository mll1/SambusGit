/*
 * SBUSDriver.cpp
 *
 *  Created on: Jan 25, 2021
 *      Author: Samuel
 */

#include "SAMBusDriver.h"
#include "stm32f3xx_hal.h"
#include "usart.h"

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
CRC_HandleTypeDef hcrc;

uint8_t Rxbuffer[2];
uint8_t Txbuffer[2];

const uint8_t SAMBUS_HEADER = 0xff;
const uint8_t SpecBytes = 4;
const uint8_t CRCBytes = 2;

const uint8_t TypeBitmask = 0b00011111;
const uint8_t AddressBitmask = 0b00001111;
const uint8_t LengthBitmask = 0b00111111;
const uint8_t MaxDataBytes = 64;

uint16_t CrC;
int HalStatus;

void nop2(){

}

void SAMBUSDriver::Init()
{



	  huart1.Instance = USART1;
	  huart1.Init.BaudRate = 38400;
	  huart1.Init.WordLength = UART_WORDLENGTH_8B;
	  huart1.Init.StopBits = UART_STOPBITS_1;
	  huart1.Init.Parity = UART_PARITY_NONE;
	  huart1.Init.Mode = UART_MODE_TX_RX;
	  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  if (HAL_UART_Init(&huart1) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  huart2.Instance = USART2;
	  huart2.Init.BaudRate = 38400;
	  huart2.Init.WordLength = UART_WORDLENGTH_8B;
	  huart2.Init.StopBits = UART_STOPBITS_1;
	  huart2.Init.Parity = UART_PARITY_NONE;
	  huart2.Init.Mode = UART_MODE_TX_RX;
	  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
	  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
	  if (HAL_UART_Init(&huart2) != HAL_OK)
	  {
	    Error_Handler();
	  }


	  //CRC

	  hcrc.Instance = CRC;
	  hcrc.Init.DefaultPolynomialUse = DEFAULT_POLYNOMIAL_ENABLE;
	  hcrc.Init.DefaultInitValueUse = DEFAULT_INIT_VALUE_ENABLE;
	  hcrc.Init.InputDataInversionMode = CRC_INPUTDATA_INVERSION_NONE;
	  hcrc.Init.OutputDataInversionMode = CRC_OUTPUTDATA_INVERSION_DISABLE;
	  hcrc.InputDataFormat = CRC_INPUTDATA_FORMAT_BYTES;
	  if (HAL_CRC_Init(&hcrc) != HAL_OK)
	  {
	    Error_Handler();
	  }
}

void SAMBUSDriver::UART_Transmit(uint8_t *Txbuffer, int DataSize)
{

	HalStatus = HAL_UART_Transmit(&huart1, Txbuffer, DataSize, 100);
}


int SAMBUSDriver::UART_Recieve(uint8_t *Rxbuffer, int BufferSize)
{
	HalStatus = HAL_UART_Receive(&huart2, Rxbuffer, BufferSize, 100);
	return HalStatus;
}


SambusFlag SAMBUSDriver::Encode(const SambusFrame &TxFrame, uint8_t *sendData, int DataSize,uint8_t *destData, int PacketSize)
{

	if (TxFrame.type > TypeBitmask){
		return SambusFlag::ERROR_FRAME_INVALID;
	}

	else if (TxFrame.address > AddressBitmask){
		return SambusFlag::ERROR_FRAME_INVALID;
	}

	else if (TxFrame.length > LengthBitmask){
		return SambusFlag::ERROR_FRAME_INVALID;
	}

	else if (DataSize > MaxDataBytes){
		return SambusFlag::ERROR_FRAME_INVALID;
	}

	int j = 0;

	destData[0] = SAMBUS_HEADER;
	destData[1] = TxFrame.type;
	destData[2] = TxFrame.address;
	destData[3] = TxFrame.length;

	for(int i = SpecBytes ; i < DataSize + SpecBytes + 1; i++){
		destData[i] = sendData[j];
		j++;
	}

	CrC = HAL_CRC_Calculate(&hcrc, (uint32_t*) destData, DataSize+4);
	uint16_t crcmsb;
	uint16_t crclsb;
	crcmsb = CrC >> 8;
	crclsb = CrC & 0x00ff;

	destData[PacketSize-2] = (uint8_t) crcmsb;
	destData[PacketSize-1] = (uint8_t) crclsb;



return SambusFlag::OK;

}


SambusFlag SAMBUSDriver::Decode(SambusFrame &RxFrame, uint8_t *recData, int DataSize, uint8_t *packetData, int PacketSize){

	if (packetData[0] != SAMBUS_HEADER){
		return SambusFlag::ERROR_NOFRAME;
	}
	if (packetData[1] > TypeBitmask){
		return SambusFlag::ERROR_FRAME_INVALID;
	}

	else if (packetData[2] > AddressBitmask){
		return SambusFlag::ERROR_FRAME_INVALID;
	}

	else if (packetData[3] > LengthBitmask){
		return SambusFlag::ERROR_FRAME_INVALID;
	}

	int j = 0;
	uint16_t CrCCheck;
	RxFrame.type = packetData[1] & TypeBitmask;
	RxFrame.address = packetData[2] & AddressBitmask;
	RxFrame.length = packetData[3] & LengthBitmask;
	PacketSize = RxFrame.length + SpecBytes + CRCBytes;
	DataSize = RxFrame.length;
	nop2();

	for(int i = SpecBytes; i < RxFrame.length + SpecBytes + 1; i++){

		recData[j] = packetData[i];
		j++;
	}
	uint16_t crcmsb = packetData[PacketSize-2];
	uint16_t crclsb = packetData[PacketSize-1];
	CrC = (crcmsb << 8) | (crclsb & 0xff);

	CrCCheck = HAL_CRC_Calculate(&hcrc, (uint32_t*) packetData, (PacketSize - CRCBytes));

	if (CrCCheck != CrC){
		return SambusFlag::ERROR_CRC;
	}

	if (RxFrame.length > MaxDataBytes){
		return SambusFlag::ERROR_FRAME_INVALID;
	}

	return SambusFlag::OK;
}


void sendData()
{

}


