/*
 * STMInterface.h
 *
 *  Created on: Jan 11, 2022
 *      Author: micha
 */

#ifndef STMINTERFACE_H_
#define STMINTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "stm32f3xx_hal.h"
#include "usart.h"

//#include "SAMBusDriver.h"

void start();

#ifdef __cplusplus
}
#endif

#endif /* STMINTERFACE_H_ */
