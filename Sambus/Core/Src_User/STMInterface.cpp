/*
 * STMInterface.cpp
 *
 *  Created on: Jan 11, 2022
 *      Author: micha
 */
#include "../Src_User/STMInterface.h"
#include "SAMBusDriver.h"

int status;

const uint8_t SpecBytes = 6;
uint8_t PacketSizeNormal;
uint8_t PacketSizeTooBig;
uint8_t PacketSizeFrameEdge;
uint8_t PacketSizeDataTooBig;



void nop(){

}


void start(){

	SAMBUSDriver Sambus;

	//Example data Encode

	//Test Normal data/ Valid packet
	SambusFrame TxFrameNormal;
	TxFrameNormal.address = 0;
	TxFrameNormal.length = 12;
	TxFrameNormal.type = 0x3;
	uint8_t sendDataNormal[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	PacketSizeNormal = TxFrameNormal.length + SpecBytes;
	uint8_t destDataNormal[PacketSizeNormal];

	//Test Frame too big
	SambusFrame TxFrameTooBig;
	TxFrameTooBig.address = 7;
	TxFrameTooBig.length = 16;
	TxFrameTooBig.type = 0b00111111;
	uint8_t sendDataFrameTooBig[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
	PacketSizeTooBig = TxFrameTooBig.length + SpecBytes;
	uint8_t destDataTooBig[PacketSizeTooBig];

	//Test Frame on edge data
	SambusFrame TxFrameFrameEdge;
	TxFrameFrameEdge.address = 0b00001111;
	TxFrameFrameEdge.length = 12;
	TxFrameFrameEdge.type = 0b00011111;
	uint8_t sendDataFrameEdge[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
	PacketSizeFrameEdge = TxFrameFrameEdge.length + SpecBytes;
	uint8_t destDataEdge[PacketSizeFrameEdge];

	//Test Data too big
	SambusFrame TxFrameDataTooBig;
	TxFrameDataTooBig.address = 0;
	TxFrameDataTooBig.length = 70;
	TxFrameDataTooBig.type = 0x3;
	uint8_t sendDataDataTooBig[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, };
	PacketSizeDataTooBig = TxFrameDataTooBig.length + SpecBytes;
	uint8_t destDataDataTooBig[PacketSizeDataTooBig];

	SambusFlag EncodeNormal = Sambus.Encode(TxFrameNormal, sendDataNormal, TxFrameNormal.length, destDataNormal, PacketSizeNormal);

	SambusFlag EncodeFrameTooBig = Sambus.Encode(TxFrameTooBig, sendDataFrameTooBig, TxFrameTooBig.length, destDataTooBig, PacketSizeTooBig);

	SambusFlag EncodeFrameEdge = Sambus.Encode(TxFrameFrameEdge, sendDataFrameEdge, TxFrameFrameEdge.length, destDataEdge, PacketSizeFrameEdge);

	SambusFlag EncodeDataTooBig = Sambus.Encode(TxFrameDataTooBig, sendDataDataTooBig, TxFrameDataTooBig.length, destDataDataTooBig, PacketSizeDataTooBig);


	nop();


	//Example data Decode
	//Normal packet
	SambusFrame RxFrameNormal;
	uint8_t recDataNormal[70];
	int RxPacketSizeNormal = 0;
	int RxDataSizeNormal = 0;

	destDataNormal[0] = 0xc2;
	destDataNormal[TxFrameNormal.length + 4] = 0xaa;
	destDataNormal[TxFrameNormal.length + 5] = 0x00;


	SambusFlag DecodeHeader  = Sambus.Decode(RxFrameNormal, recDataNormal, RxDataSizeNormal, destDataNormal, RxPacketSizeNormal);

	destDataNormal[0] = 0xff;

	SambusFlag DecodeCRC  = Sambus.Decode(RxFrameNormal, recDataNormal, RxDataSizeNormal, destDataNormal, RxPacketSizeNormal);

	nop();
	nop();
}



